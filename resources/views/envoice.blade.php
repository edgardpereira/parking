@extends('layout')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Factura</h1></div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Cliente</h4>
                            <div class="row">
                                <div class="col-md-4 text-right">Nombre:</div>
                                <div class="col-md-8">{{$car->firstname}} {{$car->lastname}}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 text-right">Identificación:</div>
                                <div class="col-md-8">{{$car->doctype}} {{$car->doc_number}}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 text-right">Dirección:</div>
                                <div class="col-md-8">{{$car->address}}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 text-right">Teléfono:</div>
                                <div class="col-md-8">{{$car->phone}}</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6 text-right">Factura N°:</div>
                                <div class="col-md-6">{{$fact}}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 text-right">Fecha:</div>
                                <div class="col-md-6">{{$desde->format('d M Y')}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row border">
                        <div class="col-md-2"><strong>Vehículo</strong></div>
                        <div class="col-md-2">Tipo: {{$car->type}}</div>
                        <div class="col-md-3">Tipo: {{$car->mark}}</div>
                        <div class="col-md-2">Tipo: {{$car->model}}</div>
                        <div class="col-md-3">Placa: {{$car->plate}}</div>
                    </div>
                    <div class="row border">
                        <div class="col-md-6">Ingreso: {{$desde}}</div>
                        <div class="col-md-6">Salida: {{$hasta}}</div>
                    </div>
                    <div class="row border">
                        <div class="col-md-6 text-right">Minutos:</div>
                        <div class="col-md-6">{{$min}}</div>
                        <div class="col-md-6 text-right">Costo/min:</div>
                        <div class="col-md-6">${{$car->cost}}</div>
                        <div class="col-md-6 text-right">Subtotal:</div>
                        <div class="col-md-6">${{$car->cost * $min}}</div>
                        <div class="col-md-6 text-right">Descuento({{$discount}}%):</div>
                        <div class="col-md-6">-${{$car->cost * $min * $discount/100}}</div>
                        <div class="col-md-6 text-right">Total a Pagar:</div>
                        <div class="col-md-6">${{$car->cost * $min - $car->cost * $min * $discount/100}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

