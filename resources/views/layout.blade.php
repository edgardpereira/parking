<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Parking</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
&nbsp;<br>
<h1 class="text-center">Sistema de Parqueadero</h1>
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
<div class="container">
        <a class="nav-link" href="/" role="button"  aria-haspopup="true" aria-expanded="false" v-pre>
            Inicio
        </a>
    </div>
    <div class="container">
        <a class="nav-link" href="/costs" role="button"  aria-haspopup="true" aria-expanded="false" v-pre>
            Configurar Precios
        </a>
    </div>
    <div class="container">
        <a class="nav-link" href="/discounts" role="button"  aria-haspopup="true" aria-expanded="false" v-pre>
            Configurar Descuentos
        </a>
    </div>
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        Reportes
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <?php $fecha =  date('Y-m-d', strtotime(date("Y-m-d"). ' - 5 hours')) ?>
                        <a class="dropdown-item" href="/report/places?startDate={{$fecha}}&endDate={{$fecha}}">Uso de Parqueadero</a>
                        <a class="dropdown-item" href="/report/vehicles?startDate={{$fecha}}&endDate={{$fecha}}">Reporte de Vehículos</a>
                        <a class="dropdown-item" href="/report/types?startDate={{$fecha}}&endDate={{$fecha}}">Tipos de Vehículos</a>
                        <a class="dropdown-item" href="/report/sales?startDate={{$fecha}}&endDate={{$fecha}}">Reporte de Ventas</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

&nbsp;<br>
@yield('content')
<footer>
    <hr>
    <p><small><i>Desarrollado por: Edgard Pereira</i></small></p>
</footer>

</body>
</html>