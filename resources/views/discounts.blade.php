@extends('layout')

@section('content')

    
    <div class="modal fade" id="edit_modal">
        <div class="modal-dialog">
            <form id="discountdata" method="post" action="">
            <input type="hidden" id="_method" name="_method" value="put" />
                <div class="modal-content">
                <div class="modal-body">
                    <h4 id='titleModal'></h4>
                    Minutos para descuento:
                    <input type="text" name="minute" id="minute" value="" class="form-control">
                    Porcentaje de descuento:
                    <input type="text" name="percent" id="percent" value="" class="form-control">
                </div>
                <div class="col-md-offset-1 col-md-10">
                    <input type="button" value="" id="submit" class="btn btn-primary btn-sm">
                </div>
                &nbsp;
            </div>
            </form>
        </div>
    </div>
    <!-- React root DOM -->
    <div id="discounts">
    </div>
<!-- React JS -->
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>

$(document).ready(function () {
    
$('body').on('click', '#editDiscount', function (event) {
    event.preventDefault();
    $("#discountdata").prop('action', "/api/discounts/"+$(this).data('id'))
    var id = $(this).data('id');
    $.get('/api/discounts/' + id + '/edit', function (data) {
         $('#titleModal').html("Editar Descuento");
         $('#submit').val("Guardar");
         $('#minute').val(data.discount.minute);
         $('#percent').val(data.discount.percent);
     })

     $('#submit').on('click', function () {
        axios.put( $("#discountdata").prop('action'), { "minute": $('#minute').val(), "percent": $('#percent').val()  } ).then(response=>{
            data = response.data
            if (data.status==200) {
                alert('La información fue actualizada correctamente')
                $("#edit_modal").modal('toggle');
                $("#min"+data.discount.id).html(data.discount.minute)
                $("#per"+data.discount.id).html(data.discount.percent)
            } else {
                alert(data.message)
            }
        }).catch(error=>{
          alert("Error "+error)
        })
     })     
});

$('body').on('click', '#addDiscount', function (event) {
    event.preventDefault();
    $("#discountdata").prop('action', "/api/discounts/")
    $('#titleModal').html("Agregar Descuento");
    $('#submit').val("Agregar");
    $('#minute').val( '' );
    $('#percent').val( '' );

     $('#submit').on('click', function () {
        axios.post( $("#discountdata").prop('action'), { "minute": $('#minute').val(), "percent": $('#percent').val(), "_method" : "post"  } ).then(response=>{
            data = response.data
            if (data.status==200) {
                alert('La información fue actualizada correctamente')
                window.location= "/discounts/"
            } else {
                alert(data.message)
            }
        }).catch(error=>{
          alert("Error "+error)
        })
     })     
});

}); 
</script>
@endsection