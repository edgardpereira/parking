@extends('layout')
<link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-alpha1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <style>
        .container {
            max-width: 500px;
        }
        h2 {
            color: white;
        }
        .mostrar {
            display: block;
            visibility: visible;
        }
        .ocultar {
            display: none;
            visibility: hidden;
        }
    </style>

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registro de Vehículo</div>

                <div class="card-body">
                    @if ($message!="") 
                        <div class="alert alert-danger" role="alert">{{$message}}</div> 
                    @endif
                <form method="post" action="/api/vehicles">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="owner_id">Cédula Propietario</label>
                            <select class="owner_id form-control" name="owner_id" required></select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <a href="/owners/new" id="btnOwner" class="btn btn-primary btn-sm mostrar"> Crear Propietario </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="type_id">Tipo de Vehículo</label>
                                <select class="form-control" name="type_id" required>
                                    <option>Seleccione...</option>
                    
                                    @foreach ($types as $item)
                                        <option value="{{ $item->id }}"> 
                                            {{ $item->name }} 
                                        </option>
                                    @endforeach    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="plate">Placa</label>
                                <input class="form-control" name="plate" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="mark">Marca</label>
                                <input class="form-control" name="mark" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                                <label for="model">Modelo</label>
                                <input class="form-control" name="model" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="btn btn-primary" value="Guardar" type="submit">
                            </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>



    <script type="text/javascript">
    $('.owner_id').select2({
        placeholder: 'Seleccione...',
        ajax: {
            url: '/api/owners/search',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.firstname+' '+item.lastname,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

</script>

@endsection

