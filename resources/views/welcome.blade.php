@extends('layout')

<div class="modal fade" id="ingreso_modal">
    <div class="modal-dialog">
        <form id="typedata" method="post" action="">
        <input type="hidden" name="_method" value="put" />
        <input type="hidden" id="place" value="" />
        <input type="hidden" id="type" value="" />
            <div class="modal-content">
            <div class="modal-body">
                <h4 id='titleModal'>Ingreso de Vehículo</h4>
                Placa:
                <input type="text" name="plate" id="plate" value="" class="form-control">
            </div>
            <div class="col-md-offset-1 col-md-10">
                <input type="button" value="Ingresar" id="submit_ingreso" class="btn btn-primary btn-sm">
            </div>
            &nbsp;
        </div>
        </form>
    </div>
</div>

<div class="modal fade" id="salida_modal">
    <div class="modal-dialog">
        <form id="form_exit" method="post" action="">
        <input type="hidden" id="splace" name="place" value="" />
            <div class="modal-content">
            <div class="modal-body">
                <h4 id='titleModal'>Salida de Vehículo</h4>
                Placa:
                <input type="text" name="plate" id="splate" value="" class="form-control">
            </div>
            <div class="col-md-offset-1 col-md-10">
                <input type="submit" value="Salida de Vehículo" id="submit_salida" class="btn btn-primary btn-sm">
            </div>
            &nbsp;
        </div>
        </form>
    </div>
</div>

@section('content')

    <!-- React root DOM -->
    <div id="places"></div>

    <!-- React JS -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
    $(document).ready(function () {
        $('#submit_ingreso').on('click', function () {
            axios.get( '/api/places/'+$("#plate").val()+'/is_parking' ).then(response=>{
                resp = response.data
                if (resp.vehicle_id==null) {
                    $("#ingreso_modal").modal('toggle');
                    alert("El vehículo no está registrado...");
                    window.location= "/vehicles/new"
                 } else {
                    if (resp.place_id==null) {
                        // Registra el ingreso
                        $("#ingreso_modal").modal('toggle');
                        if ( document.getElementById($("#place").val()).getAttribute('type_id')==resp.type_id ) {
                            axios.put('/api/places/'+$("#place").val(), { "vehicle_id": resp.vehicle_id } ).then(response=>{
                                $("#"+$("#place").val()).html( $("#plate").val().toUpperCase() );
                                document.getElementById($("#place").val()).style["background-color"] = "red";
                            }).catch(error=>{ alert("Error "+error) })
                        } else {
                            alert ('El Puesto seleccionado No coincide con el tipo de Vehículo')
                        }
                    } else {
                        alert ('El Vehículo ya se encuentra ingresado al Parqueadero')
                    }
                    
                }
            }).catch(error=>{
            alert("Error "+error)
            })
        }) 

    });
</script>

@endsection

