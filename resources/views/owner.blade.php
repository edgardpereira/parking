@extends('layout')
<link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-alpha1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <style>
        .container {
            max-width: 500px;
        }
        h2 {
            color: white;
        }
    </style>

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registro de Propietario</div>

                <div class="card-body">
                    
                       @if ($message!="") 
                        <div class="alert alert-danger" role="alert">{{$message}}</div> 
                       @endif
                    <form method="post" action="/api/owners">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="doctype_id">Tipo de Documento</label>
                                <select class="form-control" name="doctype_id" required>
                                    <option>Seleccione...</option>
                    
                                    @foreach ($doctypes as $item)
                                        <option value="{{ $item->id }}"> 
                                            {{ $item->name }} 
                                        </option>
                                    @endforeach    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="doc_number">Número de Documento</label>
                                <input class="form-control" name="doc_number" type="number" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                                <label for="firstname">Nombre</label>
                                <input class="form-control" name="firstname" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lastname">Apellido</label>
                                <input class="form-control" name="lastname" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                                <label for="address">Dirección</label>
                                <input class="form-control" name="address" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone">Teléfono</label>
                                <input class="form-control" name="phone" type="number" required>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="btn btn-primary" value="Guardar" type="submit">
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



    <script type="text/javascript">
    $('.doc_number').select2({
        placeholder: 'Seleccione...',
        ajax: {
            url: '/api/owners/search',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.firstname+' '+item.lastname,
                            id: item.id
                        }
                    })
                };
                if ( $("#select2-doc_number-1a-container options").lenght != 0 ) {
                    $("#btnOwner").addClass("ocultar")
                    $("#btnOwner").removeClass("mostrar")
                } else {
                    $("#btnOwner").addClass("mostrar")
                    $("#btnOwner").removeClass("ocultar")
                }
            },
            cache: true
        }
    });
</script>

@endsection

