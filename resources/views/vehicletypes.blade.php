@extends('layout')

@section('content')

    
    <div class="modal fade" id="edit_modal">
        <div class="modal-dialog">
            <form id="typedata" method="post" action="">
            <input type="hidden" name="_method" value="put" />
                <div class="modal-content">
                <div class="modal-body">
                    <h4 id='titleModal'></h4>
                    <input type="text" name="cost" id="cost" value="" class="form-control">
                </div>
                <div class="col-md-offset-1 col-md-10">
                    <input type="button" value="" id="submit" class="btn btn-primary btn-sm">
                </div>
                &nbsp;
            </div>
            </form>
        </div>
    </div>
    <!-- React root DOM -->
    <div id="vehicletypes">
    </div>
<!-- React JS -->
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>

$(document).ready(function () {
    
$('body').on('click', '#editCost', function (event) {
    event.preventDefault();
    $("#typedata").prop('action', "/api/vehicletypes/"+$(this).data('id'))
    var id = $(this).data('id');
    $('#titleModal').html("Nueva tarifa para "+$(this).data('name'));
    $('#submit').val("Guardar");
    $('#cost').val( $(this).data('cost'));

     $('#submit').on('click', function () {
        axios.put( $("#typedata").prop('action'), { "cost": $('#cost').val() } ).then(response=>{
            data = response.data
            if (data.status==200) {
                alert('La información fue actualizada correctamente')
                $("#edit_modal").modal('toggle');
                $("#cost"+data.type.id).html(data.type.cost)

            } else {
                alert(data.message)
            }
        }).catch(error=>{
          alert("Error "+error)
        })
     })     
});

}); 
</script>
@endsection