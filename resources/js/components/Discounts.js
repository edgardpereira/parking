import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Discounts extends Component {
    
    constructor(props){
        super(props);
        this.state = {
          discounts: []
        }
      }
  
      componentDidMount(){
  
        axios.get('/api/discounts').then(response=>{
          this.setState({discounts: response.data.discounts})
        }).catch(error=>{
          alert("Error "+error)
        })
  
      }
  
      render() {
          return (
            <div className="container">
              <br/>
              <h3>Descuentos Activos
                <div className='text-right'>
                <a className='btn btn-info' href="" id="addDiscount" data-toggle="modal" data-target='#edit_modal'>Nuevo Decuento</a>
                </div>
              </h3>
              <hr/>
              
              <table className="table table-bordered order-table">
                <thead>
                  <tr>
                    <th>Minutos</th>
                    <th>Porcentaje</th>
    
                  </tr>
                </thead>
                <tbody id="bodytable">
                    {this.renderList()}
                </tbody>
              </table>
  
              

            </div>
          );
      }
  
      renderList(){
        return this.state.discounts.map((data)=>{
          return(
            <tr key={data.id} id={'row'+data.id}>
              <td id={'min'+data.id}>{data.minute}</td>
              <td id={'per'+data.id}>{data.percent}</td>
              <td> 
              <a className='btn btn-info' href="" id="editDiscount" data-toggle="modal" data-target='#edit_modal' data-id={ data.id }>Editar</a>
              </td>
           
            </tr>
          )
  
        })
  
      }
  }

if (document.getElementById('discounts')) {
    ReactDOM.render(<Discounts />, document.getElementById('discounts'));
}