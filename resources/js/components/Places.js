import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Places extends Component {
    
    constructor(props){
        super(props);
        this.state = {
          places: []
        }
      }
  
      componentDidMount(){
  
        axios.get('/api/places').then(response=>{
          this.setState({places: response.data.places})
        }).catch(error=>{
          alert("Error "+error)
        })
  
      }
  
      render() {
          return (
            <div className="container">
              <div className="row justify-content-center">
                  <div className="col-md-8">
                      <div className="card">
                          <div className="card-header">
                            <div className="row">
                              <div className="col-md-3 text-right">Ingrese Placa:</div>
                              <div className="col-md-2"><input id="placa" type='text' /></div>
                              <div className="col-md-2"></div>
                              <div className="col-md-2">
                                <div onClick={(e) => this.entra()} className="btn btn-primary">Ingresar</div>
                                </div>
                              <div className="col-md-2">
                                <div onClick={(e) => this.sale()} className="btn btn-primary">Salida</div>
                              </div>
                            </div>
                          </div>

                          <div className="card-body">

                          {this.renderList()}

                          </div>
                      </div>
                  </div>
              </div>
          </div>
          );
      }

      btnParking(data) {
        $("#place").val(data.id);
        $("#type").val(data.type_id);
        if ( document.getElementById($("#place").val()).style["background-color"]=='green' ) { 
          $("#plate").val("");
          $("#ingreso_modal").modal('toggle');
        } else {
          $("#salida_modal").modal('toggle');
          $("#splate").val( $("#"+$("#place").val() ).html() )
          $("#splace").val( $("#place").val() )
          $("#form_exit").prop('action', "/api/places/"+$("#place").val()+"/envoice")
        }
      }

      entra() {
        let placa = $("#placa").val();
        if (placa.trim()=="") {
          alert("Debe seleccionar una placa primero")
          return 0;
        } 
        axios.get('/api/places/'+$("#placa").val()+'/is_parking').then(resp=>{
          if (resp.data.place_id!=null) {
            alert("El vehículo ya se encuentra en el Parqueadero");
            return 0;
          }
          if (resp.data.vehicle_id!=null) {
            axios.get('/api/places/'+resp.data.type_id+'/place_free').then(resp2=>{
              // Registra el ingreso
              axios.put('/api/places/'+resp2.data, { "vehicle_id": resp.data.vehicle_id } ).then(response=>{
                $("#"+resp2.data).html( $("#placa").val().toUpperCase() );
                document.getElementById(resp2.data).style["background-color"] = "red";
              });
            });
          } else {
            window.location= "/vehicles/new"
          }
        });
      }

      sale() {
        let placa = $("#placa").val();
        if (placa.trim()=="")  {
          alert("Debe seleccionar una placa primero");
          return 0
        }
        axios.get('/api/places/'+$("#placa").val()+'/is_parking').then(resp=>{
          if (resp.data.place_id==null) {
            alert("El vehículo NO se encuentra en el Parqueadero");
            return 0;
          } else {
            window.location = "/api/places/"+resp.data.place_id+"/envoice";
          }
        });
      }
  
      renderList(){
        let primero = 0;
        let carros1 = this.state.places.filter(data => data.type_id == 3 && data.is_front == false);
        let carros2 = this.state.places.filter(data => data.type_id == 3 && data.is_front == true);
        let bicis = this.state.places.filter(data => data.type_id == 2);
        let motos1 = this.state.places.filter(data => data.type_id == 1 && data.is_front == false);
        let motos2 = this.state.places.filter(data => data.type_id == 1 && data.is_front == true);
        let M1 = motos1.map((data)=>{
          var style = data.vehicle_id==null ? {"backgroundColor": "green"} : {"backgroundColor": "red"};
          style.color = "white"
          var text = data.vehicle_id==null ? "M "+data.id : data.vehicle.plate;
           return (
           <div className="col-md-1 border" style={style} id={data.id} type_id={data.type_id} key={data.id} onClick = {(e) => this.btnParking(data)}>{text}</div>
           )
        });
        let M2 = motos2.map((data)=>{
          var style = data.vehicle_id==null ? {"backgroundColor": "green"} : {"backgroundColor": "red"};
          style.color = "white"
          var text = data.vehicle_id==null ? "M "+data.id : data.vehicle.plate;
           return (
           <div className="col-md-1 border" style={style} id={data.id} type_id={data.type_id} key={data.id} onClick = {(e) => this.btnParking(data)}>{text}</div>
           )
        });
        let B = bicis.map((data)=>{
          var style = data.vehicle_id==null ? {"backgroundColor": "green"} : {"backgroundColor": "red"};
          style.color = "white"
          var text = data.vehicle_id==null ? "B"+data.id : data.vehicle.plate;
           return (
           <div className="col-md-1 border" style={style} id={data.id} type_id={data.type_id}  key={data.id} onClick = {(e) => this.btnParking(data)}>{text}</div>
           )
        });
        let C2 = carros2.map((data)=>{
          var style = data.vehicle_id==null ? {"backgroundColor": "green"} : {"backgroundColor": "red"};
          style.color = "white"
          var text = data.vehicle_id==null ? "C "+data.id : data.vehicle.plate;
           return (
           <div className="col-md-2 border text-center" style={style} id={data.id} type_id={data.type_id} key={data.id} onClick = {(e) => this.btnParking(data)}>{text}</div>
           )
        });
        let C1 = carros1.map((data)=>{
          var style = data.vehicle_id==null ? {"backgroundColor": "green"} : {"backgroundColor": "red"};
          style.color = "white"
          var text = data.vehicle_id==null ? "C "+data.id : data.vehicle.plate;
          return (
           <div className="col-md-2 border text-center" style={style} id={data.id} type_id={data.type_id} key={data.id} onClick = {(e) => this.btnParking(data)}>{text}</div>
           )
        });
        return (<>
        <div className="row"><div className="col-md-1">&nbsp;</div>{M1}</div>
        <div className="row"><div className="col-md-1">&nbsp;</div>{M2}</div>
        <div>&nbsp;</div>
        <div className="row"><div className="col-md-1">&nbsp;</div>{B}</div>
        <div>&nbsp;</div>
        <div className="row"><div className="col-md-1">&nbsp;</div>{C2}</div>
        <div className="row"><div className="col-md-1">&nbsp;</div>{C1}</div>
        </>)
      }
}

if (document.getElementById('places')) {
    ReactDOM.render(<Places />, document.getElementById('places'));
}

