import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Vehicletypes extends Component {
    
    constructor(props){
        super(props);
        this.state = {
          vehicletypes: []
        }
      }
  
      componentDidMount(){
  
        axios.get('/api/vehicletypes').then(response=>{
          this.setState({vehicletypes: response.data.types})
        }).catch(error=>{
          alert("Error "+error)
        })
  
      }
  
      render() {
          return (
            <div className="container">
              <br/>
              <h3>Tarifa por Tipo de Vehículo</h3>
              <hr/>
              
              <table className="table table-bordered order-table">
                <thead>
                  <tr>
                    <th>Tipo</th>
                    <th>Costo/min</th>
    
                  </tr>
                </thead>
                <tbody id="bodytable">
                    {this.renderList()}
                </tbody>
              </table>
  
              

            </div>
          );
      }
  
      renderList(){
        return this.state.vehicletypes.map((data)=>{
          return(
            <tr key={data.id}>
              <td>{data.name}</td>
              <td id={'cost'+data.id}>{data.cost}</td>
              <td> 
              <a className='btn btn-info' href="" id="editCost" data-toggle="modal" data-target='#edit_modal' data-id={ data.id } data-name={ data.name } data-cost={ data.cost }>Editar</a>
              </td>
           
            </tr>
          )
  
        })
  
      }
  }

if (document.getElementById('vehicletypes')) {
    ReactDOM.render(<Vehicletypes />, document.getElementById('vehicletypes'));
}