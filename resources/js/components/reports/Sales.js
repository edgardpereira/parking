import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import queryString from 'query-string'

import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';

export default class ReportsSales extends Component {
    
    constructor(props){
        let params = queryString.parse(window.location.search);
        super(props);
        this.state = {
            data: [],
            startDate: (params == {} ? new Date() : new Date(""+params.startDate+" 00:00:00") ),
            endDate: (params == {} ? new Date() : new Date(""+params.endDate+" 23:59:59") )
        }
        this.handleChangeB = this.handleChangeB.bind(this);
        this.handleChangeE = this.handleChangeE.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }
    
    handleChangeB(date) { this.setState({ startDate: date }) }
    handleChangeE(date) { this.setState({ endDate: date }) }
        
    onFormSubmit(e) { 
        e.preventDefault(); 
        window.location.href = "/report/sales?startDate="+fechaf(this.state.startDate)+"&endDate="+fechaf(this.state.endDate)
    }

    componentDidMount(){
        let params = queryString.parse(window.location.search);
        let urlParam = (params == {} ? '' : '?startDate='+params.startDate+'&endDate='+params.endDate )
        axios.post('/api/report/sales'+urlParam).then(response=>{
            console.log(params)
            console.log(fechaf(new Date()))
            this.setState({
                data: response.data
            })
        }).catch(error=>{
            alert("Error "+error)
        })
    }
  
    render() {
        return (
            <div className="container">
                <br/>
                <h3>Reporte de Facturación</h3>
                <hr/>
                <form onSubmit={ this.onFormSubmit } action="/report/sales" method="get" id="fechas">
                    <div className="row">
                        <div className="col-md-3">
                            <div className="form-group"><DatePicker selected={ this.state.startDate } 
                                onChange={ this.handleChangeB }
                                dateFormat="yyyy-MM-dd" className="form-control"
                            /></div>
                        </div>
                        <div className="col-md-3">
                            <div className="form-group"><DatePicker selected={ this.state.endDate } 
                                onChange={ this.handleChangeE }
                                dateFormat="yyyy-MM-dd" className="form-control"
                            /></div>
                        </div>
                       
                        <div className="col-md-2"><button className="btn btn-primary">Filtrar</button></div>
                    </div>
                </form>
                <div className='table-responsive'>
                    <table className='table table-bordered order-table'>
                        <thead>
                            <tr><th>Fecha</th><th>Monto</th></tr>
                        </thead>
                        <tbody>
                            {this.renderList()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
  
    renderList(){
        let type = ['','Moto','Bici','Carro'];
        const options2 = { style: 'currency', currency: 'USD' };
        const numberFormat2 = new Intl.NumberFormat('en-US', options2);
        return this.state.data.map((data)=>{
          return(
            <tr key={data.fecha}>
              <td>{data.fecha}</td>
              <td>{numberFormat2.format(data.total)}</td>
            </tr>
          )
        })
  
    }
}

if (document.getElementById('reports_sales')) {
    ReactDOM.render(<ReportsSales />, document.getElementById('reports_sales'));
}

function fechaf(f) {
    return f.getFullYear() + "-"  + ("0" + eval(f.getMonth() + 1)).slice(-2) + "-" + ("0" + f.getDate()).slice(-2)
}

