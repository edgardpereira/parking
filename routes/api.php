<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('vehicletypes', 'VehicletypesController');
Route::resource('discounts', 'DiscountsController');
Route::resource('vehicles', 'VehiclesController');
Route::get('vehicles/find/{plate}', 'VehiclesController@find');
Route::put('/places/{id}',  'PlacesController@update');
Route::get('places', 'PlacesController@index');
Route::get('places/{plate}/is_parking', 'PlacesController@is_parking');
Route::get('places/{type}/place_free', 'PlacesController@place_free');
Route::match(['GET', 'POST'], '/places/{id}/envoice', 'PlacesController@envoice');
Route::get('owners/search', 'OwnersController@search');
Route::post('/owners',  'OwnersController@store');

Route::post( 'report/places',   'HistoricsController@places');
Route::post( 'report/vehicles', 'HistoricsController@vehicles');
Route::post( 'report/types',    'HistoricsController@types');
Route::post( 'report/sales',    'HistoricsController@sales');

