<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/discounts', function () {
    return view('discounts');
});

Route::get('/costs', function () {
    return view('vehicletypes');
});

Route::get('/vehicles/new', 'VehiclesController@create');
Route::get( '/owners/new', 'OwnersController@create');

Route::get( '/report/places',   function () { return view('reports.places'); });
Route::get( '/report/vehicles', function () { return view('reports.vehicles'); });
Route::get( '/report/types',    function () { return view('reports.types'); });
Route::get( '/report/sales',    function () { return view('reports.sales'); });
