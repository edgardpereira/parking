<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicletypes')->insert([ 'name' => 'Moto', 'cost' => 1 ]);
        DB::table('vehicletypes')->insert([ 'name' => 'Bicicleta', 'cost' => 2 ]);
        DB::table('vehicletypes')->insert([ 'name' => 'Carro', 'cost' => 3 ]); 

        DB::table('doctypes')->insert([ 'name' => 'CC']);
        DB::table('doctypes')->insert([ 'name' => 'CE']);
        DB::table('doctypes')->insert([ 'name' => 'NIT']);

        DB::table('places')->insert([ 'type_id' => 1, 'position' => 0, 'is_front' => false]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 1, 'is_front' => false]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 2, 'is_front' => false]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 3, 'is_front' => false]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 4, 'is_front' => false]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 5, 'is_front' => false]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 6, 'is_front' => false]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 7, 'is_front' => false]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 8, 'is_front' => false]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 9, 'is_front' => false]);

        DB::table('places')->insert([ 'type_id' => 1, 'position' => 0, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 1, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 2, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 3, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 4, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 5, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 6, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 7, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 8, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 1, 'position' => 9, 'is_front' => true]);
        
        DB::table('places')->insert([ 'type_id' => 2, 'position' => 0, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 2, 'position' => 1, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 2, 'position' => 2, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 2, 'position' => 3, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 2, 'position' => 4, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 2, 'position' => 5, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 2, 'position' => 6, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 2, 'position' => 7, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 2, 'position' => 8, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 2, 'position' => 9, 'is_front' => true]);

        DB::table('places')->insert([ 'type_id' => 3, 'position' => 0, 'is_front' => false]);
        DB::table('places')->insert([ 'type_id' => 3, 'position' => 1, 'is_front' => false]);
        DB::table('places')->insert([ 'type_id' => 3, 'position' => 2, 'is_front' => false]);
        DB::table('places')->insert([ 'type_id' => 3, 'position' => 3, 'is_front' => false]);
        DB::table('places')->insert([ 'type_id' => 3, 'position' => 4, 'is_front' => false]);

        DB::table('places')->insert([ 'type_id' => 3, 'position' => 0, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 3, 'position' => 1, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 3, 'position' => 2, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 3, 'position' => 3, 'is_front' => true]);
        DB::table('places')->insert([ 'type_id' => 3, 'position' => 4, 'is_front' => true]);

    }
}
