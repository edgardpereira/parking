<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historics', function (Blueprint $table) {
            $table->id();
            $table->foreignId('place_id')->constrained('places');
            $table->foreignId('type_id')->constrained('vehicletypes');
            $table->foreignId('vehicle_id')->constrained('vehicles');
            $table->datetime('time_begin');
            $table->datetime('time_end');
            $table->integer('minutes');
            $table->double('cost_per_minute');
            $table->integer('discount');
            $table->double('total_value');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historics');
    }
}
