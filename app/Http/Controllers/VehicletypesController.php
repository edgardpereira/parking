<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehicletype;

class VehicletypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Vehicletype::orderBy('name', 'ASC')->get();
        return response() -> json(['status' => 200, 'types' => $types]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = Vehicletype::find($id);
        return response()->json(['status' => 200, 'type' => $type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = Vehicletype::find($id);
        $type->cost = $request->cost;
        if($type -> save()){
            return response()->json(["status" => 200, "type" => $type ]);
        }
    }

}
