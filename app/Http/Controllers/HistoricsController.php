<?php

namespace App\Http\Controllers;

use App\Models\Historic;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class HistoricsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function places(Request $request) {
        $startDate = date('Y-m-d', strtotime(date("Y-m-d"). ' - 5 hours'));
        $endDate   = date('Y-m-d', strtotime(date("Y-m-d"). ' - 5 hours'));
        if (isset($request->startDate)) $startDate = $request->startDate;
        if (isset($request->endDate)) $endDate = $request->endDate;
        $endDate = date('Y-m-d', strtotime($endDate. ' + 1 days'));
        $sqlStr = "select place_id,type_id,count(place_id) cant from historics where (DATE_ADD(time_begin, INTERVAL 5 HOUR) between '$startDate 05:00:00' and '$endDate 04:59:59') or (DATE_ADD(time_end, INTERVAL 5 HOUR) between '$startDate 05:00:00' and '$endDate 04:59:59') group by place_id, type_id order by count(place_id) DESC;";
        $data = DB::select( $sqlStr );
        return response() -> json($data);
        //return response() -> json(['data'=>$data, 'startDate'=>$startDate, 'endDate'=>$endDate, 'sqlStr'=>$sqlStr]);
    }

    public function vehicles(Request $request) {
        $startDate = date('Y-m-d', strtotime(date("Y-m-d"). ' - 5 hours'));
        $endDate   = date('Y-m-d', strtotime(date("Y-m-d"). ' - 5 hours'));
        if (isset($request->startDate)) $startDate = $request->startDate;
        if (isset($request->endDate)) $endDate = $request->endDate;
        $endDate = date('Y-m-d', strtotime($endDate. ' + 1 days'));
        $sqlStr = "select h.id,t.name,plate,h.place_id,DATE_SUB(time_begin, INTERVAL 5 HOUR) time_begin,DATE_SUB(time_end, INTERVAL 5 HOUR) time_end from historics h INNER JOIN vehicles v on h.vehicle_id=v.id INNER JOIN vehicletypes t ON h.type_id=t.id where (DATE_ADD(time_begin, INTERVAL 5 HOUR) between '$startDate 05:00:00' and '$endDate 04:59:59') or (DATE_ADD(time_end, INTERVAL 5 HOUR) between '$startDate 05:00:00' and '$endDate 04:59:59') order by vehicle_id DESC;";
        $data = DB::select( $sqlStr );
        return response() -> json($data);
    }

    public function types(Request $request) {
        $startDate = date('Y-m-d', strtotime(date("Y-m-d"). ' - 5 hours'));
        $endDate   = date('Y-m-d', strtotime(date("Y-m-d"). ' - 5 hours'));
        if (isset($request->startDate)) $startDate = $request->startDate;
        if (isset($request->endDate)) $endDate = $request->endDate;
        $endDate = date('Y-m-d', strtotime($endDate. ' + 1 days'));
        $sqlStr = "SELECT t.name, COUNT(h.type_id) cant from historics h INNER JOIN vehicletypes t ON (h.type_id=t.id) where (DATE_ADD(time_begin, INTERVAL 5 HOUR) between '$startDate 05:00:00' and '$endDate 04:59:59') or (DATE_ADD(time_end, INTERVAL 5 HOUR) between '$startDate 05:00:00' and '$endDate 04:59:59') GROUP BY t.name;";
        $data = DB::select( $sqlStr );
        return response() -> json($data);
    }

    public function sales(Request $request) {
        $startDate = date('Y-m-d', strtotime(date("Y-m-d"). ' - 5 hours'));
        $endDate   = date('Y-m-d', strtotime(date("Y-m-d"). ' - 5 hours'));
        if (isset($request->startDate)) $startDate = $request->startDate;
        if (isset($request->endDate)) $endDate = $request->endDate;
        $endDate = date('Y-m-d', strtotime($endDate. ' + 1 days'));
        $sqlStr = "SELECT DATE_FORMAT(time_end, '%d-%m-%Y') fecha, SUM(total_value) total FROM historics where (DATE_ADD(time_begin, INTERVAL 5 HOUR) between '$startDate 05:00:00' and '$endDate 04:59:59') or (DATE_ADD(time_end, INTERVAL 5 HOUR) between '$startDate 05:00:00' and '$endDate 04:59:59') GROUP BY DATE_FORMAT(time_end, '%d-%m-%Y');";
        $data = DB::select( $sqlStr );
        return response() -> json($data);
    }
  
}
