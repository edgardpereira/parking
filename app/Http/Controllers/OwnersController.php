<?php

namespace App\Http\Controllers;

use App\Models\Owner;
use App\Models\DocType;
use Illuminate\Http\Request;

class OwnersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function search(Request $request)
    {
    	$owners = [];
        if($request->has('q')){
            $search = $request->q;
            $owners =Owner::select("id", "firstname", "lastname")
            		->where('doc_number', 'LIKE', "%$search%")
            		->get();
        }
        return response()->json($owners);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $doctypes = Doctype::select('id','name')->get();
        return view('owner', ["doctypes" => $doctypes , "message" => "" ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'doctype_id' => 'required',
            'doc_number' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ]);
        if (Owner::firstWhere('doc_number', $validatedData['doc_number']) == null) {
            $data = Owner::create([
                'doctype_id' => $validatedData['doctype_id'],
                'doc_number' => $validatedData['doc_number'],
                'firstname' => $validatedData['firstname'],
                'lastname' => $validatedData['lastname'],
                'address' => $validatedData['address'],
                'phone' => $validatedData['phone'],
            ]);
            return view('vehicle');
        } else {
            $doctypes = Doctype::select('id','name')->get();
            return view('owner', ["doctypes" => $doctypes , "message" => "Ya existe un propietario con el número de cédula" ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function show(Owner $owner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function edit(Owner $owner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Owner $owner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Owner $owner)
    {
        //
    }
}
