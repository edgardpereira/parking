<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Discount;

class DiscountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $discounts = Discount::orderBy('minute', 'ASC')->get();
        return response() -> json(['status' => 200, 'discounts' => $discounts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'minute' => 'required',
            'percent' => 'required',
        ]);
        if (Discount::firstWhere('minute', $validatedData['minute']) == null) {
            $discount = Discount::create([
                'minute' => $validatedData['minute'],
                'percent' => $validatedData['percent'],
            ]);
            return response() -> json(['status' => 200, 'discount' => $discount]);
        } else {
            return response() -> json(['status' => 422, 'message' => "ERROR: Ya existe un descuento..."]);
        }
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $discount = Discount::find($id);
        return response() -> json(['status' => 200, 'discount' => $discount]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $discount = Discount::find($id);
        if ($discount != null) {
            $updatedata = true;
            if ($discount->minute != $request->minute && Discount::firstWhere('minute', $request->minute) != null) {
                $updatedata = false;
            }
            if ($updatedata) {
                $discount->minute = $request->minute;
                $discount->percent = $request->percent;
                $discount->save();
                return response() -> json(['status' => 200, 'discount' => $discount]);
            } else {
                return response() -> json(['status' => 422, 'message' => "ERROR: Ya existe un descuento..."]);
            }
            
        } else {
            return response() -> json(['status' => 422, 'message' => "ERROR: No existe el descuento..."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Discount::find($id);
        $item->delete();
        return response() -> json(['status' => 200, 'message' => "Se ha eliminado el descuento"]);
    }
}
