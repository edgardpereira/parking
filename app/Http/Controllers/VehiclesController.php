<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use App\Models\Vehicletype;
use Illuminate\Http\Request;

class VehiclesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Vehicletype::select('id','name')->get();
        return view('vehicle', ["types" => $types , "message" => "" ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'owner_id' => 'required',
            'plate' => 'required',
            'type_id' => 'required',
            'mark' => 'required',
            'model' => 'required',
        ]);
        if (Vehicle::firstWhere('plate', $validatedData['plate']) == null) {
            $data = Vehicle::create([
                'owner_id' => $validatedData['owner_id'],
                'type_id' => $validatedData['type_id'],
                'plate' => strtoupper($validatedData['plate']),
                'mark' => $validatedData['mark'],
                'model' => $validatedData['model'],
            ]);
            return view('welcome');
        } else {
            $types = Vehicletype::select('id','name')->get();
            return view('vehicle', ["types" => $types , "message" => "Ya existe un vehículo con el número de Placa" ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Vehicle::find($id);
        return response()->json(['status' => 200, 'data' => $data]);
    }

    public function find($plate)
    {
        $data = Vehicle::firstWhere('plate', strtoupper($plate) );
        return response()->json(['status' => 200, 'data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicle $vehicle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle)
    {
        //
    }
}
