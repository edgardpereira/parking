<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Place;
use App\Models\Discount;
use App\Models\Owner;
use App\Models\Vehicle;
use App\Models\Vehicletype;
use App\Models\Historic;

class PlacesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $places = Place::query()
        ->with(array('vehicle' => function($query) {
            $query->select('id','plate');
        }))->orderBy('type_id', 'asc')->orderBy('is_front', 'asc')->orderBy('position', 'asc')->get();
        return response() -> json(['status' => 200, 'places' => $places]);
    }

    public function is_parking($plate) {
        $car = Vehicle::where('plate', strtoupper($plate))->first();
        $place = null;
        $type = null;
        if ($car!=null) {
            $place = Place::where('vehicle_id', $car->id )->first();
            $type = $car->type_id;
        }
        return response() -> json([
            'vehicle_id' => $car!=null ? $car->id : null,
            'place_id' => $place!=null ? $place->id : null,
            'type_id' => $type
        ]);
    }

    public function place_free($type) {
        return $this->get_free($type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Place::find($id);
        $data->vehicle_id = $request->vehicle_id;
        $data->since = date("Y-m-d H:i:s");
        $data -> save();
        return response()->json(["status" => 200, "data" => $data ]);
    }

    public function envoice(Request $request, $id)
    {
        $data = Place::find($id);
        if ($data->vehicle_id == null) { return view('welcome'); }

        $this->check_block_car($data);

        $desde = new \Carbon\Carbon($data->since);
        $hasta = new \Carbon\Carbon(date("Y-m-d H:i:s"));
        $min=$desde->diffInMinutes($hasta);

        $discount = Discount::where('minute','<=' , $min)->orderBy('minute', 'desc')->select('percent')->first();
        if ($discount == null) {
            $desc =  0;
        } else {
           $desc = $discount->percent;
        }
        $car = Vehicle::query()
        ->with(array('type' => function($query) {
            $query->select('name');
        }))->find($data->vehicle_id);

        $car = Vehicle::join("vehicletypes","vehicles.type_id","=","vehicletypes.id")
        ->join("owners","vehicles.owner_id","=","owners.id")
        ->join("doctypes","doctype_id","=","doctypes.id")
        ->where('vehicles.id','=',$data->vehicle_id)
        ->selectRaw('vehicles.*, vehicletypes.name type, firstname, lastname, 
        address, phone, doc_number, doctypes.name doctype, cost')
        ->first();
        $fact = 1;

        $data->vehicle_id = null;
        $data->since = null;
        $data->save();

        Historic::create([
            "type_id" => $data->type_id,
            "place_id" => $data->id,
            "vehicle_id" => $car->id, 
            "minutes" => $min, 
            "time_begin" => $desde, 
            "time_end" => $hasta, 
            "cost_per_minute" => $car->cost, 
            "discount" => $desc, 
            "total_value" => $car->cost*$min - $car->cost*$min*$desc/100
        ]);
        
        return view('envoice', ["status" => 200, "place" => $data, 'fact'=>$fact,
            'desde'=>$desde, 'hasta'=>$hasta, 'min'=>$min, 'discount'=>$desc, 'car'=>$car
        ]);
    }

    private function check_block_car($data) {
        if ($data->type_id==2) return true;
        if ($data->is_front==true) return true;
        
        $car_front = Place::where('position',$data->position)
                      ->where('is_front',true)
                      ->where('type_id',$data->type_id)->first();
        if  ($car_front->vehicle_id == null) return true;

        $place_free_id = $this->get_free($data->type_id);
        if ($place_free_id != -1) {
            $this->change_position( $car_front, $place_free_id);
            return true;
        }
        $this->change_position( $data, $car_front->id );
        return true;
    }

    private function change_position($place1, $new_position) {
        $car_id = $place1->vehicle_id;
        $fecha = $place1->desde;
        $place2 = Place::find($new_position);
        
        $place1->vehicle_id = $place2->vehicle_id;
        $place1->since = $place2->since;
        $place1->save();

        $place2->vehicle_id = $car_id;
        $place2->since = $fecha;
        $place2->save();
        return true;
    }

    private function get_free($type) {
        $place_free = Place::where('type_id',$type)
        ->where('is_front',false)
        ->where('vehicle_id',null)->get();
        if ($place_free->count()>0) {
            $new_place = random_int(0, $place_free->count()-1 );
            return $place_free->toArray()[$new_place]['id'];
        }
        if ($type != 2) {
            $place_free = Place::where('type_id',$type)
            ->where('is_front',true)
            ->where('vehicle_id',null)->get();
            if ($place_free->count()>0) {
                $new_place = random_int(0, $place_free->count()-1 );
                return $place_free->toArray()[$new_place]['id'];
            }
        }
        return -1;
    }

}
