<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historic extends Model
{
    use HasFactory;
    protected $fillable = ["type_id","place_id","vehicle_id", "minutes", "time_begin", "time_end", "cost_per_minute", "discount", "total_value"];

    public function type()
    {
        return $this->belongsTo(Vehicletype::class);
    }

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function place()
    {
        return $this->belongsTo(Place::class);
    }
}
