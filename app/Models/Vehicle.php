<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;
    protected $fillable = ["plate","type_id", "owner_id", "mark", "model"];

    public function type()
    {
        return $this->belongsTo(Vehicletype::class);
    }

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }
}
