<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    use HasFactory;
    protected $fillable = ["firstname","lastname", "doctype_id", "doc_number", "address", "phone"];

    public function doctype()
    {
        return $this->belongsTo(Doctype::class);
    }
}
