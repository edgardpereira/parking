<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    use HasFactory;
    protected $fillable = ["type_id","position", "is_front", "vehicle_id", "since"];

    public function type()
    {
        return $this->belongsTo(Vehicletype::class);
    }

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }
}
